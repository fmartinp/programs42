
Set of programs for Free42. They can be used with HP-42s, Free42 and DM42. The programs use the extended functions of Free42, so some adaptation is required for HP-42s.

Information on the usage of the functions is included embedded the source code.

Compatible with Free42 3.0.0

MAIN, MATH, HYP, DATES, UNIT, CPX, CONSTANT, CALC, DEBUG

#### MAIN program

Main menu of the Application suite. It can always be called using XEQ 99

#### MATH program

Menu for some math functions. So far, the purpose of this function is to
create a CUSTOM menu with the top row shifted funcions.
The menu adds a the LGyX function to compute base-y logarithm.

#### HYP program

Menu to hyperbolic trigonometric functions.

#### DATES program

Collection of date related functions accessible through a CUSTOM menu.
List of functions:
 - DDAYS (Free42 builtin function): Compute number of days between 2 dates
 - DATE+ Free42 builtin function): Compute the addition of a date and number of days
 - D→DOY: Convert a date into the day of the year
 - DOY→D: Convert a day of the year into a date
 - D→YWD: Convert a date into a year, ISO week and day of the week
 - YWD→D: Convert a year, ISO week and day of the week in a date
 - UTIME: Compute current UNIX time (based on Thomas Okken's program: https://thomasokken.com/free42/42progs/unixtime.txt)
 - T→U: Convert a date into UNIX time (based on Thomas Okken's program: https://thomasokken.com/free42/42progs/unixtime.txt)
 - U→T: Convert a UNIX time into a date (based on Thomas Okken's program: https://thomasokken.com/free42/42progs/unixtime.txt)
 - GTIME: Compute current GPS time
 - T→G: Convert a date into GPS time (GPS week and seconds of the week)
 - G→T: Conver GPS time (GPS week and seconds of the week) into a date
 - SDATE: Compute current ESA Solar Orbiter mission planning cycle
 - D→STP: Convert a date into a ESA Solar Orbiter mission planning cycle
 - STP→D: Convert a ESA Solar Orbiter mission planning cycle into a date

Note: The convention of days of the week is 1-Monday, 2-Tuesday, 3-Wednesday, 4-Thursday, 5-Friday, 6-Saturday, 0-Sunday

#### CALC program

CALC program
Collection of calculus related functions accessible through a CUSTOM menu.
List of functions:
 - DERIV: Compute the derivative of a selected order and accuracy of a MVAR function (Interactive app)
 - TAYLR: Compute the taylor expansion of a selected order and accuracy of a MVAR function (Interactive app)
 - SUMN: Compute the sum of a serie specified by MVAR function (Interactive app)

#### UNIT converter program

Collection of unit conversion functions accessible through a CUSTOM menu.
List of functions and units:
 - LENG: meter, inch, yard, mile, nautic mile
 - AREA: square meter, square foot, acre, hectometer, square kilometer, square mile
 - MASS: kilogram, dram, onze, pound, stone, carat
 - VOL: liter, fluid onze, gill, pint, quart, gallon
 - SPEED: m/s, km, mph, knot, ft/s, in/s
 - TEMP: Celsius, Kelvin and Farenheit degrees
 - Time: second, minute, hour, day, sideral day, week
 - ENGY: Joule, Calorie, Watt per hour, eV
 - PWER: Watt, Electrical (E), Mechanical (I) and Metric (M) horsepower
Usage:
 - Select the unit applicable for the X-register and afterwards select the conversion unit.
 - If no new X-register is introduced with the keyboard, the current value in X-register
   as it has already assigned a unit, it can be converted to a new unit just pressing the
   unit name in the soft menu
 - Alternative use, if a unit is pressed twice, the unit is selected and any new value in
   X-register is automatically converted to that unit once a unit is assigned to the value.
   This behaviour is disabled pressing over the selected unit.

#### CPX program

Menu to some complex functions. So far, the purpose of this function is to
create a CUSTOM menu with some commons complex funcions.

The program adds the following functions:
 - CONJ: Computes the conjugate of a complex number
 - R<>P: Swaps between rectangular and polar modes

#### CONSTANT Library program

Constant Libray implemented through a programmable menu.

#### DEBUG program

Menu for some debug functions following Thomas Okken recommendation at:
https://thomasokken.com/free42/