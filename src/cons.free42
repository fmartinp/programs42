//  *****************************************************************************
//  * Application Suite for Free42 -- a set of applications for Free42
//  * Copyright (C) 2020-2021  Fernando Martin
//  *
//  * This program is free software; you can redistribute it and/or modify
//  * it under the terms of the GNU General Public License, version 2,
//  * as published by the Free Software Foundation.
//  *
//  * This program is distributed in the hope that it will be useful,
//  * but WITHOUT ANY WARRANTY; without even the implied warranty of
//  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  * GNU General Public License for more details.
//  *
//  * You should have received a copy of the GNU General Public License
//  * along with this program; if not, see http://www.gnu.org/licenses/.
//  *****************************************************************************

// CONSTANT Library program
// Constant Libray implemented through a programmable menu.
// Values extracted from https://physics.nist.gov/cuu/Constants/ (2018 database)
// Constants available:
// c: speed light, 299792458 m s-1
// Eo: vacuum dielectric contant, 8.8541878128ᴇ-12 F m-1
// µo: vacuum permeability constant, 1.25663706212ᴇ-6 N A-2
// Zo: vaccum impedance, 376.730313668 ohm
// k↓b: Boltzman's constant, 1.380649ᴇ-23 J K-1
// h: Plank's constant, 6.62607015ᴇ-34 J Hz-1
// G: Newton's Gravity constant, 6.67430ᴇ-11 m3 kg-1 s-2
// g: standard Earth's gravity, 9.80665 m s-2
// e-: electron charge, 1.602176634ᴇ-19 C
// Na: Avogadro Number, 6.02214076ᴇ23 mol-1
// mp+: Proton mass, 1.67262192369ᴇ-27 kg
// me-: Electron mass, 9.1093837015ᴇ-31 kg

// Compatible with Free42 2.5.24

LBL "CONS"

LBL A
"c"
KEY 1 XEQ 01
"Eo"
KEY 2 XEQ 02
"µo"
KEY 3 XEQ 03
"Zo"
KEY 4 XEQ 04
"k↓b"
KEY 5 XEQ 05
"h"
KEY 6 XEQ 06
KEY 7 GTO B
KEY 8 GTO B
KEY 9 GTO 99
MENU
LBL 90
STOP
GTO 90

LBL B
"G"
KEY 1 XEQ 07
"g"
KEY 2 XEQ 08
"e-"
KEY 3 XEQ 09
"Na"
KEY 4 XEQ 10
"mp+"
KEY 5 XEQ 11
"me-"
KEY 6 XEQ 12
KEY 7 GTO A
KEY 8 GTO A
KEY 9 GTO 99
LBL 91
STOP
GTO 91


LBL 01
299792458
XEQ F
⊢" m s-1"
AVIEW
RTN

LBL 02
8.8541878128ᴇ-12
XEQ F
⊢" F m-1"
AVIEW
RTN

LBL 03
1.25663706212ᴇ-6
XEQ F
⊢" N A-2"
AVIEW
RTN

LBL 04
376.730313668
XEQ F
⊢" ohm"
AVIEW
RTN

LBL 05
1.380649ᴇ-23
XEQ F
⊢" J K-1"
AVIEW
RTN

LBL 06
6.62607015ᴇ-34
XEQ F
⊢" J Hz-1"
AVIEW
RTN

LBL 07
6.67430ᴇ-11
XEQ F
⊢" m3 kg-1 s-2"
AVIEW
RTN

LBL 08
9.80665 
XEQ F
⊢" m s-2"
AVIEW
RTN

LBL 09
1.602176634ᴇ-19
XEQ F
⊢" C"
AVIEW
RTN

LBL 10
6.02214076ᴇ23
XEQ F
⊢" mol-1"
AVIEW
RTN

LBL 11
1.67262192369ᴇ-27
XEQ F
⊢" kg"
AVIEW
RTN

LBL 12
9.1093837015ᴇ-31
XEQ F
⊢" kg"
AVIEW
RTN

LBL F
CLA
ARCL ST X
RTN

LBL 99
CLMENU
GTO "MAIN"
LBL "_CONS"